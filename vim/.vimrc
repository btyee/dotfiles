set nocompatible   " Vim is improved!
filetype on        " I'm told this is required
syntax enable

"colorscheme evening

set mouse=a

" Turn on line numbers
set number

" Turn on spell check
" set spell

" Set tabs soft and 4x by default
set softtabstop=4
set tabstop=4
set expandtab
set shiftwidth=4
set smarttab
set smartindent

" automatically reload edited file
set autoread

" Make searching more friendly
set ignorecase
set smartcase
set incsearch
set hlsearch

" Anything more than 80 characters is unwise
set wrap
set linebreak
set colorcolumn=80

" Make backspace behave more like other programs
set backspace=indent,eol,start

" Set status line to something more useful
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]

" Simplify pane-switching
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Set preference for where splits occur
set splitbelow
set splitright
