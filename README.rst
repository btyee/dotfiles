These are my personal dotfiles. Use is as simple as cloning this project to your
home directory, entering the directory, and issuing `stow` for all applicable
programs. You can also use the init script to do this for all included configs.
