# command prompt
export PS1="\[\033[38;5;220m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\[$(tput sgr0)\]\[\033[38;5;75m\]\h\[$(tput sgr0)\]\[\033[38;5;75m\]:\w\[$(tput sgr0)\]\[\033[38;5;75m\] \[$(tput sgr0)\]\[\033[38;5;7m\]\\$\[$(tput sgr0)\]\[\033[38;5;75m\] \[$(tput sgr0)\]"

# COLORS PRETTY COLORS
alias grep='grep --color=auto'
alias ls='ls -G'

# system-specific configurations
HOSTNAME=$( hostname )
if [ "$HOSTNAME" = Indulgence.lan ]; then
    # use keychain to manage ssh and gpg keys
    eval `keychain --eval --agents ssh,gpg --inherit any id_ed25519 1CB59117`
    # turn on completions for pass
    if [ -f $(brew --prefix)/etc/bash_completion ]; then
        . $(brew --prefix)/etc/bash_completion
    fi
    export GNUPGHOME=$HOME/Sync/.gnupg
    export PASSWORD_STORE_DIR=$HOME/Sync/.password-store
elif [ "$HOSTNAME" = hosako ]; then
    export GNUPGHOME=$HOME/Sync/.gnupg
    export PASSWORD_STORE_DIR=$HOME/Sync/.password-store
fi

export PATH="$HOME/.cargo/bin:$PATH"

[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion
