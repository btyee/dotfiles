#!/usr/bin/env bash

for PROGRAM in $( ls -d */ ); do
    stow -t "$HOME" $PROGRAM
done
